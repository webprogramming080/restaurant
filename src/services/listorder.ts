import type Listorder from "@/types/Listorder";
import http from "./axios";

function getListlistorders() {
  return http.get("/listlistorders");
}
function getListorderbyid(id: number) {
  return http.get(`/listorders/${id}`);
}
function saveListorder(order: {
  // tables: number;
  ListorderItem: { menuId: number; amount: number }[];
}) {
  return http.post("/listorders", order);
}
function updateListorder(id: number, order: Listorder) {
  return http.patch(`/listorders/${id}`, order);
}
function deleteListorder(id: number) {
  return http.delete(`/listorders/${id}`);
}

function getListorderBytable(id: number) {
  return http.get(`/listorders/table/${id}`);
}

export default {
  getListlistorders,
  deleteListorder,
  updateListorder,
  saveListorder,
  getListorderBytable,
  getListorderbyid,
};
