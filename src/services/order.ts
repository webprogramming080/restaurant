import type Order from "@/types/Order";
import http from "./axios";
import type Table from "@/types/Table";
import table from "./table";
import orderItem from "./orderItem";

function getOrders() {
  return http.get("/orders");
}
function getOrderbyId(id: number) {
  return http.get(`/orders/${id}`);
}
function saveOrder(order: {
  // tables: number;
  OrderItem: { menuId: number; amount: number }[];
}) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}
function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

function getOrderBytable(id: number) {
  return http.get(`/orders/table/${id}`);
}

export default {
  getOrders,
  deleteOrder,
  updateOrder,
  saveOrder,
  getOrderBytable,
  getOrderbyId,
};
