import type OrderItem from "@/types/OrderItem";
import http from "./axios";

function getOrderItems() {
  return http.get("/order-Items");
}
// function getOrderbyid(id: number) {
//   return http.get(`/orders/${id}`);
// }
function saveOrderItems(orderItems: OrderItem) {
  return http.post("/order-Items", orderItems);
}
function updateOrderItems(id: number, orderItem: OrderItem) {
  return http.patch(`/order-Items/${id}`, orderItem);
}
function deleteOrderItems(id: number) {
  return http.delete(`/order-Items/${id}`);
}

// function getOrderBytable(id: number) {
//   return http.get(`/orders/table/${id}`);
// }

export default {
  getOrderItems,
  deleteOrderItems,
  updateOrderItems,
  saveOrderItems,
  // getOrderBytable,
  // getOrderbyid,
};
