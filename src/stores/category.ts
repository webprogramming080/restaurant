import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import categoryService from "@/services/category";
import { useMessageStore } from "./message";

export const useCategoryStore = defineStore("Category", () => {
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const categories = ref<Category[]>([]);
  const editedCategory = ref<Category>({ name: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCategory.value = {
        name: "",
      };
    }
  });

  async function getCategories() {
    try {
      const res = await categoryService.getCategories();
      categories.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Category ได้");
    }
  }

  async function saveCategory() {
    // loadingStore.isLoading = true;
    try {
      if (editedCategory.value.id) {
        const res = await categoryService.updateCategory(
          editedCategory.value.id,
          editedCategory.value
        );
      } else {
        const res = await categoryService.saveCategory(editedCategory.value);
      }

      dialog.value = false;
      await getCategories();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Category ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  function editCategory(category: Category) {
    editedCategory.value = JSON.parse(JSON.stringify(category));
    dialog.value = true;
  }

  return {
    categories,
    dialog,
    editedCategory,
    saveCategory,
    editCategory,
    getCategories,
  };
});
