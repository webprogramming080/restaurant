import { ref } from "vue";
import { defineStore } from "pinia";

export const useKitchenStore = defineStore("kitchen", () => {
  const chefDialog = ref(false);
  const selectedChef = ref("");
  const chefs = ["เชฟชุมพล", "เชฟป้อม", "เชฟเอียน"];
  return { chefDialog, selectedChef, chefs };
});
