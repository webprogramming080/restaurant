import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Listorder from "@/types/Listorder";
import type ListTables from "@/types/ListTables";
import type Menu from "@/types/Menu";
import type Table from "@/types/Table";

export const useTables = defineStore("ListTables", () => {
  const list = ref<ListTables[]>([
    { id: 1, table: "1" },
    { id: 2, table: "2" },
    { id: 3, table: "V1" },
  ]);

  function removeTableById(_tableId: number) {
    const index = list.value.findIndex((item) => item.id === _tableId);
    list.value.splice(index, 1);
  }

  return {
    list,
    removeTableById,
  };
});

export const useListOrder = defineStore("Listorder", () => {
  // const dialog = ref(false);
  const isTable = ref(false);
  const tableId = ref(-1);
  // const editedServe = ref<Serve>({ id: "", name: "", quantity: "" });
  // id: number;
  // name: string;
  // quantity: number;
  const list = ref<Listorder[]>([
    { id: 1, table: 1, name: "ราดหน้าปลาดุกกุ๊กๆกี้", quantity: 2 },
    { id: 2, table: 1, name: "ไข่เจียวนกฟามิงโก้", quantity: 1 },
    { id: 3, table: 1, name: "ชาเย็นแห่งแอตแลนติก", quantity: 3 },
    { id: 5, table: 2, name: "ชาเย็นแห่งแอตแลนติก", quantity: 3 },
    { id: 6, table: 3, name: "ชาเย็นแห่งแอตแลนติก", quantity: 3 },
    { id: 7, table: 3, name: "ไข่เจียวนกฟามิงโก้", quantity: 3 },
  ]);

  const listByTableId = ref<Listorder[]>([]);

  const updateListByTableId = (_tableId: number): void => {
    console.log("อัพเดทรายการไปที่โต๊ะ:", _tableId);

    // listByTableId = list.value.filter((item) => item.table === _tableId);
    listByTableId.value = list.value.filter((item) => item.table === _tableId);
    tableId.value = _tableId;
  };

  // const deleteListorder = (id: number): void => {
  //   // console.log("list data:", list.value);
  //   const index = list.value.findIndex((item) => item.id === id);
  //   console.log("delete index", index);
  //   list.value.splice(index, 1);
  //   updateListByTableId(tableId.value);
  // };

  function removeOrderByTableId(_tableId: number) {
    console.log("Remove order in table:", _tableId);
    list.value = list.value.filter((item) => item.table !== _tableId);
    console.log("Result order list:", list.value);
  }

  const orderList = ref<
    { menu: Menu; amount: number; sum: number; table?: Table }[]
  >([]);
  function deleteListorder(index: number) {
    orderList.value.splice(index, 1);
  }

  // function clearListOrder() {
  //   orderList.value = [];
  // }

  return {
    list,
    isTable,
    tableId,
    listByTableId,
    updateListByTableId,
    deleteListorder,
    // clearListOrder,
    // deleteListorder,
    removeOrderByTableId,
  };
});
