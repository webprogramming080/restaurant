import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import menuService from "@/services/menu";
import { useMessageStore } from "./message";

export const useMenuStore = defineStore("Menu", () => {
  const mainPage = ref(false);
  const secondPage = ref(false);
  const messageStore = useMessageStore();
  const menus = ref<Menu[]>([]);
  const category = ref(1);
  const dialog = ref(false);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMenu.value = {
        name: "",
        price: 0,
        categoryId: 1,
        image: "No_Image_Available.png",
        files: [],
      };
    }
  });

  watch(category, async (newCategory, oldCategory) => {
    await getMenusByCategory(newCategory);
  });

  async function getMenusByCategory(category: number) {
    try {
      const res = await menuService.getMenusByCategory(category);
      menus.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Menu ได้");
    }
  }
  const editedMenu = ref<Menu & { files: File[] }>({
    name: "",
    price: 0,
    categoryId: 1,
    image: "No_Image_Available.png",
    files: [],
  });
  function editMenu(menu: Menu) {
    editedMenu.value = JSON.parse(JSON.stringify(menu));
    console.log(editedMenu.value.name);
    dialog.value = true;
  }

  async function saveMenu() {
    // loadingStore.isLoading = true;
    try {
      if (editedMenu.value.id) {
        const res = await menuService.updateMenu(
          editedMenu.value.id,
          editedMenu.value
        );
      } else {
        const res = await menuService.saveMenu(editedMenu.value);
      }

      dialog.value = false;

      await getMenus();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Menu ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }
  async function deleteMenu(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await menuService.deleteMenu(id);
      await getMenus();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Menu ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function getMenus() {
    try {
      const res = await menuService.getMenus();
      menus.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Menu ได้");
    }
  }

  const orderList = ref<{ menu: Menu; amount: number; sum: number }[]>([]);
  function addMenu(item: Menu) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].menu.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        return;
      }
    }
    console.log("hello");
    orderList.value.push({ menu: item, amount: 1, sum: 1 * item.price });
  }

  function deleteMenuOrder(index: number) {
    orderList.value.splice(index, 1);
  }

  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });

  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  return {
    menus,
    editMenu,
    mainPage,
    secondPage,
    getMenus,
    saveMenu,
    deleteMenu,
    editedMenu,
    dialog,
    category,
    getMenusByCategory,
    deleteMenuOrder,
    sumPrice,
    sumAmount,
    addMenu,
    orderList,
  };
});
