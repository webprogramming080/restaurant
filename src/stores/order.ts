import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import { useMessageStore } from "./message";
import type Order from "@/types/Order";
import orderService from "@/services/order";
import table from "@/services/table";
import type Table from "@/types/Table";

export const useOrderStore = defineStore("Order", () => {
  // const count = ref(0);
  // const doubleCount = computed(() => count.value * 2);
  // function increment() {
  //   count.value++;
  // }
  const table = ref(1);
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const alert = ref(false);
  const orders = ref<Order[]>([]);
  // const editedOrder = ref<Order>({
  //   tableId: 1,
  //   table: [],
  // });

  const hello = () => {
    console.log("Hello");
  };

  watch(table, async (newTable, oldTable) => {
    await getOrdersByTable(newTable);
  });

  async function getOrdersByTable(table: number) {
    try {
      const res = await orderService.getOrderBytable(table);
      orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล order ได้");
    }
  }

  async function changeOrdersByTable(id: number, order: Order, table: Table) {
    try {
      order.tableId = table.id;
      const res = await orderService.updateOrder(id, order);
      orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถอัพเดทข้อมูล order ได้");
    }
  }

  const orderList = ref<{ menu: Menu; amount: number; sum: number }[]>([]);
  const addMenu = (item: Menu) => {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].menu.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        return;
      }
    }
    console.log("hello");
    orderList.value.push({
      menu: item,
      amount: 1,
      sum: 1 * item.price,
    });
  };
  const close = () => {
    dialog.value = false;
  };
  function deleteMenu(index: number) {
    orderList.value.splice(index, 1);
  }

  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });

  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  // watch(dialog, (newDialog, oldDialog) => {
  //   if (!newDialog) {
  //     editedOrder.value = {};
  //   }
  // });

  async function getOrders() {
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
  }

  async function openOrder(table: Table) {
    const orderItems = orderList.value.map(
      (item) =>
        <{ menuId: number; amount: number }>{
          menuId: item.menu.id,
          amount: item.amount,
        }
    );
    const order = { orderItems: orderItems, tableId: table.id, status: "wait" };
    console.log(order);
    // console.log(table);
    try {
      const res = await orderService.saveOrder(order);
      dialog.value = false;
      clearOrder();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Order ได้");
    }
  }

  function clearOrder() {
    orderList.value = [];
  }
  // async function saveOrder() {
  //   try {
  //     if (editedOrder.value.id) {
  //       const res = await orderService.updateOrder(
  //         editedOrder.value.id,
  //         editedOrder.value
  //       );
  //     } else {
  //       const res = await orderService.saveOrder(editedOrder.value);
  //     }
  //     dialog.value = false;
  //     await getOrders();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึก Order ได้");
  //   }
  // }

  const deleteOrder = (id: number): void => {
    const index = orders.value.findIndex((item) => item.id === id);
    orders.value.splice(index, 1);
  };

  // const editOrder = (order: Order) => {
  //   editedOrder.value = { ...order };
  //   dialog.value = true;
  // };

  // const getOrders = async () => {
  //   try {
  //     const res = await orderService.getOrders();
  //     orders.value = res.data;
  //     return orders.value;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  return {
    hello,
    orderList,
    addMenu,
    deleteMenu,
    sumPrice,
    sumAmount,
    dialog,
    deleteOrder,
    // editOrder,
    // saveOrder,
    openOrder,
    close,
    clearOrder,
    orders,
    alert,
    getOrders,
    changeOrdersByTable,
  };
});
