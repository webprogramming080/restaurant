import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import { useMessageStore } from "./message";
import type Order from "@/types/Order";
import orderItemService from "@/services/orderItem";
import orderItem from "@/services/orderItem";
import type OrderItem from "@/types/OrderItem";

export const useOrderItemStore = defineStore("OrderItems", () => {
  const orderItems = ref<OrderItem[]>([]);
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const orderBill = ref<{ menu: Menu; amount: number; price: number }[]>([]);
  const editedOrderItem = ref<OrderItem>({
    name: "",
    amount: "",
    status: "",
    tableId: -1,
    chef: "",
  });

  const chef = "";

  async function getOrderItems() {
    try {
      const res = await orderItemService.getOrderItems();
      orderItems.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  function editOrderItem(orderItem: OrderItem) {
    editedOrderItem.value = JSON.parse(JSON.stringify(orderItem));
    console.log(editedOrderItem.value.status);
    dialog.value = true;
  }

  // const addOrderItems = ไอมุขี้เกียจทำปุ่มใหญ่ ใครทำได้บ้างมาทำให้หน่อย

  // const deleteOrderItems = (id: number): void => {
  //   console.log("orderItems data:", orderItems.value);
  //   const index = orderItems.value.findIndex((item) => item.id === id);
  //   console.log("delete index", index);
  //   orderItems.value.splice(index, 1);
  // };

  const deleteOrderItem = async (id: number) => {
    try {
      const res = await orderItemService.deleteOrderItems(id);
      await getOrderItems();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ orderItem ได้");
    }
  };
  // console.log("orderItems data:", orderItems.value);
  // const index = orderItems.value.findIndex((item) => item.id === id);
  // console.log("delete index", index);
  // orderItems.value.splice(index, 1);

  function clearListOrder() {
    orderItems.value = [];
  }

  async function saveOrderItem(orderItem: OrderItem) {
    console.log(orderItem.id);
    //editedOrderItem.value.id มัน undefined ว่ะงง
    console.log(editedOrderItem.value.id);
    // loadingStore.isLoading = true;
    try {
      if (orderItem.id) {
        orderItem.status = "wait-cooking";
        console.log(orderItem.status);
        const res = await orderItemService.updateOrderItems(
          orderItem.id,
          orderItem
        );
      }
      //  else {
      //   const res = await orderItemService.saveOrderItems(
      //     editedOrderItem.value
      //   );
      // }
      console.log(orderItems);
      dialog.value = false;
      await getOrderItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก OrderItem ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  async function saveOrderItemCooking(orderItem: OrderItem) {
    try {
      if (orderItem.id) {
        // orderItem.chef = chef;
        console.log(orderItem.status);
        const res = await orderItemService.updateOrderItems(
          orderItem.id,
          orderItem
        );
      }
      //  else {
      //   const res = await orderItemService.saveOrderItems(
      //     editedOrderItem.value
      //   );
      // }
      console.log(orderItems);
      dialog.value = false;
      await getOrderItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก OrderItem ได้");
      console.log(e);
    }
  }
  async function saveOrderItemServe(orderItem: OrderItem) {
    try {
      if (orderItem.id) {
        orderItem.status = "serve";
        // orderItem.chef = chef;
        console.log(orderItem.status);
        const res = await orderItemService.updateOrderItems(
          orderItem.id,
          orderItem
        );
      }
      //  else {
      //   const res = await orderItemService.saveOrderItems(
      //     editedOrderItem.value
      //   );
      // }
      console.log(orderItems);
      dialog.value = false;
      await getOrderItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก OrderItem ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }
  async function saveOrderItemCheckbill(orderItem: OrderItem) {
    try {
      if (orderItem.id) {
        orderItem.status = "wait-checkbill";
        // orderItem.chef = chef;
        console.log(orderItem.status);
        const res = await orderItemService.updateOrderItems(
          orderItem.id,
          orderItem
        );
      }
      //  else {
      //   const res = await orderItemService.saveOrderItems(
      //     editedOrderItem.value
      //   );
      // }
      console.log(orderItems);
      dialog.value = false;
      await getOrderItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก OrderItem ได้");
      console.log(e);
    }
  }

  const sumPrice = computed(() => {
    let sum = 0;
    // for (let i = 0; i < orderItems.value.length; i++) {
    //   sum = 0; //orderItems.value[i].priceยังทำไม่ได้ติดตรงมัน.priceเพราะpriceเป็น?
    // }
    sum = sum + orderItems.value.length;
    return sum;
  });

  return {
    orderItems,
    getOrderItems,
    deleteOrderItem,
    clearListOrder,
    saveOrderItem,
    editOrderItem,
    editedOrderItem,
    saveOrderItemCooking,
    chef,
    saveOrderItemServe,
    saveOrderItemCheckbill,
    sumPrice,
  };
});
