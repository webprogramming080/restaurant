import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Serve from "@/types/Serve";
import type Menu from "@/types/Menu";

export const useServeStore = defineStore("serve", () => {
  const isTable = ref(true);
  const dialog = ref(false);
  const served = ref(false);
  const tabs = ref(false);
  const tabs2 = ref(false);
  let i = 0;
  let lastId = 4;
  const editedServe = ref<Serve>({
    id: -1,
    name: "",
    table: -1,
    amount: -1,
    status: "",
    date: "",
  });
  const serves = ref<Serve[]>([
    // {
    //   id: 1,
    //   name: "สุกี้ไก่",
    //   table: 1,
    //   amount: 1,
    //   status: "พร้อมเสิร์ฟ",
    //   date: "4/3/2566 12:00",
    // },
    // {
    //   id: 2,
    //   name: "ไก่เต้นรำบำ",
    //   table: 1,
    //   amount: 1,
    //   status: "พร้อมเสิร์ฟ",
    //   date: "4/3/2566 12:10",
    // },
    // {
    //   id: 3,
    //   name: "หมูนารี",
    //   table: 1,
    //   amount: 1,
    //   status: "พร้อมเสิร์ฟ",
    //   date: "4/3/2566 12:13",
    // },
  ]);

  // const deleteServe = (id: number): void => {
  //   const index = serves.value.findIndex((item) => item.id === id);
  //   serves.value.splice(index, 1);
  // };

  const close = () => {
    dialog.value = false;
  };

  const deleteServe = () => {
    const index = serves.value.findIndex(
      (item) => item.id === editedServe.value.id
    );
    editedServe.value.status = "เสิร์ฟแล้ว";
    serves.value.splice(index, 1);
    // editedServe.value.status = "เสิร์ฟแล้วpy'";
    dialog.value = false;
  };

  const saveServe = () => {
    if (editedServe.value.id < 0) {
      editedServe.value.id = lastId++;
      serves.value.push(editedServe.value);
    } else {
      const index = serves.value.findIndex(
        (item) => item.id === editedServe.value.id
      );
      editedServe.value.status = "เสิร์ฟแล้ว";
      i++;
      if (i == serves.value.length) {
        deleteServe();
        // clear();
      }
      // served.value = true;
      serves.value[index] = editedServe.value;
    }
    dialog.value = false;
    // if (editedServe.value.status == "เสิร์ฟแล้ว") {
    //   served.value = true;
    // }
  };

  const editServe = (serve: Serve) => {
    editedServe.value = { ...serve };
    dialog.value = true;
  };

  const clear = () => {
    editedServe.value = {
      id: -1,
      name: "",
      table: -1,
      amount: -1,
      status: "",
      date: "",
    };
  };

  return {
    serves,
    tabs,
    tabs2,
    isTable,
    dialog,
    served,
    close,
    deleteServe,
    editServe,
    saveServe,
    clear,
  };
});
