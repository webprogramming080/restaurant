import type Table from "@/types/Table";
import { defineStore } from "pinia";
import { ref } from "vue";
import "@vuepic/vue-datepicker/dist/main.css";
import type Order from "@/types/Order";
import orderService from "@/services/order";
import bookServices from "@/services/table";
import { useMessageStore } from "./message";

export const useTableStore = defineStore("table", () => {
  const tables = ref<Table[]>([]);
  const selectedTable = ref<Table>({
    id: 0,
    seat: 0,
    table_name: "",
    status: "",
    date: "",
    time: "",
  });

  const dialogEditTable = ref(false);
  const dialogChangeTable = ref(false);
  const dialogPayment = ref(false);
  const dialogCanPayment = ref(false);
  const dialogฺBilling = ref(false);
  const dialogฺClean = ref(false);
  const dialogTable = ref(false);
  const dialogBusy = ref(false);
  const dialogAdd = ref(false);
  const dialogFood = ref(false);
  const date = ref(new Date());
  const time = ref();
  const isFree = ref(false);
  const order = ref<Order[]>([]);
  const dialogOrder = ref(false);
  const messageStore = useMessageStore();

  //axios
  const getTables = async () => {
    try {
      const res = await bookServices.getTables();
      tables.value = res.data;
      return tables.value;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Tables ได้");
    }
  };

  // const setSelectedTable = async (tableId: number) => {
  //   try {
  //     const res = await bookServices.getTableById(tableId);
  //     if (res) {
  //       selectedTable.value = res.data;
  //     }
  //     return tables.value;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  async function getOrderByTable(id: number) {
    try {
      const res = await orderService.getOrderBytable(id);
      order.value = res.data;
      console.log(order.value);
    } catch (e) {
      console.log(e);
    }
  }

  const saveTable = async () => {
    try {
      if (selectedTable.value.id) {
        const res = await bookServices.updateTable(
          selectedTable.value.id,
          selectedTable.value
        );
      } else {
        const res = await bookServices.saveTable(selectedTable.value);
      }

      dialogTable.value = false;
      dialogEditTable.value = false;
      await getTables();
      clear();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Tables ได้");
    }
  };

  const deleteTable = async (id: number) => {
    // loadingStore.isLoading = true;
    try {
      const res = await bookServices.deleteTable(id);
      await getTables();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Table ได้");
    }
    // loadingStore.isLoading = false;
  };

  const editTable = (table: Table) => {
    console.log(table);

    selectedTable.value = JSON.parse(JSON.stringify(table));
    dialogTable.value = true;
    dialogEditTable.value = true;
  };

  const editTableBusy = (table: Table) => {
    selectedTable.value = table;
    dialogBusy.value = true;
  };

  const changeTable = (table1: Table, table2: Table) => {
    //if table !=busy will change
    if (table2.status !== "busy") {
      //changeTable data
      table2.status = table1.status;
      table2.date = table1.date;
      table2.time = table1.time;
      //save new table (2)
      bookServices.updateTable(table2.id, table2);
      //clear old table (1)
      table1.status = "available";
      table1.date = "";
      table1.time = "";
      bookServices.updateTable(table1.id, table1);
    }
  };

  // clear data
  const clear = async () => {
    selectedTable.value = {
      id: 0,
      seat: 0,
      table_name: "",
      status: "",
      date: "",
      time: "",
    };
  };

  return {
    dialogEditTable,
    tables,
    dialogTable,
    selectedTable,
    saveTable,
    clear,
    date,
    time,
    editTable,
    isFree,
    order,
    getOrderByTable,

    getTables,
    dialogOrder,
    dialogBusy,
    editTableBusy,
    dialogAdd,
    dialogฺBilling,
    dialogPayment,
    dialogฺClean,
    dialogCanPayment,
    dialogChangeTable,
    changeTable,
    dialogFood,
    deleteTable,
  };
});
