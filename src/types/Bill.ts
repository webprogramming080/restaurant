export default interface Bill {
  id: number;
  list: string;
  price: number;
}
