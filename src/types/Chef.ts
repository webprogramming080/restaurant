import type OrderItem from "./OrderItem";

export default interface Category {
  id?: number;
  name?: string;
  orderItems?: OrderItem[];
}
