export default interface Listorder {
  id: number;
  table: number;
  name: string;
  quantity: number;
}
