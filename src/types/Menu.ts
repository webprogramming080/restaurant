import type Category from "./Category";

export default interface Menu {
  id?: number;
  name: string;
  price: number;
  image?: string;
  categoryId: number;
  category?: Category;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
