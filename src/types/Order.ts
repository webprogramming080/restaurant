import type OrderItem from "./OrderItem";
import type Table from "./Table";
// import type Table from "./Table";

export default interface Order {
  id?: number;
  amount?: number;
  total?: number;
  tableId: number;
  status?: string;
  tables?: Table;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  orderItems?: OrderItem[];
}
