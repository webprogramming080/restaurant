import type Menu from "./Menu";
import type Order from "./Order";

export default interface OrderItem {
  id?: number;
  name?: string;
  price?: number;
  total?: number;
  amount?: string;
  status?: string;
  tableId?: number;
  chef?: string;
  orders?: Order;
  menu?: Menu;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
