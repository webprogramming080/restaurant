export default interface Serve {
  id: number;
  name: string;
  table: number;
  amount: number;
  status: string;
  date: string;
}
