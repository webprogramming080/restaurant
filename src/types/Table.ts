import type Order from "./Order";

export default interface Table {
  id?: number;
  seat: number;
  table_name: string;
  status?: string;
  date?: string; // ใส่ ? ไว้เพื่อให้ตัวแปรนี้สามารถมีค่าเป็น null ได้ จะไม่ error
  time?: string; // ใส่ ? ไว้เพื่อให้ตัวแปรนี้สามารถมีค่าเป็น null ได้ จะไม่ error
  order?: Order[];
  // id?: number;
  // table: string;
  // quantity: string;
  // status: string;
  // date: string;
  // time: string;
  // name: string;
  // phone: string;
  // cusTotal: string;

  // createdAt?: Date;
  // updatedAt?: Date;
  // deletedAt?: Date;
}
